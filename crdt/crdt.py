from dataclasses import dataclass, field
from typing import overload, List, Callable, TypeVar
from collections import UserDict

from .sites import Site
from .dag import DAG
from .op import Op, Literal
from .utils import flatten


Key = TypeVar("Key", bound=tuple[Site, str])


class Rights(UserDict[Key, DAG]):
    def __setitem__(self, key: Key, value: DAG):
        super().__setitem__(key, value)
        for item in self.values():
            item.rights = self
        for node in value.nodes:
            value.nodes[node]["key"] = key

    @property
    def nodes(self):
        return list(dict.fromkeys(flatten([list(j) for j in [i.nodes for i in self.values()]])))

    def draw(self):
        for key in self.keys():
            graph = DAG(self.get(key))
            graph.draw()


@dataclass
class CRDTp:
    site_from: Site
    num = 0
    ac_dags: Rights = field(repr=False, default_factory=Rights)  # access control dags
    waiting_pool: List[Op] = field(repr=False, default_factory=list)

    def __post_init__(self):
        for right in ["admin", "write", "read"]:
            self.ac_dags[self.site_from.label, right] = DAG(key=(self.site_from.label, right))

    def prepare(self, site_to: Site, right: Literal["admin", "write", "read"]) -> Op:
        deps = ["0"]
        sign = "-"  # type: Literal["+", "-"]
        key = (site_to.label, right)
        if dag := self._ensure_tuple_is_present(key):  # cas où le tuple d'accès existe déjà
            deps = [op for op in dag._level_nodes(dag._longest_valid_path_length()) if dag._get_valide(op)]
            sign = self.ac_dags[key]._get_sign(deps[0])

        self.num += 1  # le numéro d'opération émise par le site courant est global
        return Op(site_from=self.site_from, site_to=site_to, num=self.num, right=right, sign=sign, deps=deps)

    @overload  # quand l'intégration est mise en attente
    def effect(self, op: Op) -> None:
        ...

    @overload  # quand l'opération a eu lieu
    def effect(self, op: Op) -> List[Op]:  # type: ignore
        ...

    def effect(self, op: Op) -> None | List[Op]:
        ret = []
        self._ensure_tuple_is_present(op.tuple)

        # condition réutilisée plusieurs fois
        deps_ok: Callable[[Op], bool] = lambda _op: all([str(dep) in self.ac_dags[_op.tuple].pol for dep in _op.deps])

        # si toutes les deps d'op n'ont pas déjà été intégrées, on met en attente
        if not deps_ok(op):
            self.waiting_pool.append(op)  # mise en attente
            return None  # ! pas de signal si en attente
        elif op in self.waiting_pool:
            self.waiting_pool.remove(op)

        # on applique l'effet
        self.ac_dags[op.tuple].add_op(
            str(op),
            [str(dep) for dep in op.deps],
            last=str(op.last),
            missing=[str(dep) for dep in op.missing] if op.missing else None,
        )
        ret += [op]

        # on applique l'effet des successeurs en attente
        if any(deps_ok(ready_successor := waiting) for waiting in self.waiting_pool) and (
            successor_ret := self.effect(
                ready_successor
            )  # la récursivité nous permet de ne pas avoir à rafraîchir `self.waiting_pool` à chaque itération de ce qui auraît autrement pû se faire via une boucle
        ):
            ret += successor_ret

        return ret  # ! liste de tuples (opération intégrée, validité lors de l'intégration)

    def _ensure_tuple_is_present(self, key) -> DAG | None:
        try:
            return self.ac_dags[key]
        except KeyError:
            self.ac_dags[key] = DAG(key=key)
            return None
