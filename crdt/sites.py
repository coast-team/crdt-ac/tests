from functools import total_ordering
from typing import overload, Union


@total_ordering
class Site:
    """Classe responsable de la compraison d'ordre lexicographique entre deux sites"""

    def __init__(self, label: str, order: list[str]):
        self.label = label
        self.order = order

    def __repr__(self) -> str:
        return f"Site({self.label})"

    @overload
    def __eq__(self, other: "Site") -> bool:
        ...

    @overload
    def __eq__(self, other: str) -> bool:
        ...

    def __eq__(self, other: Union["Site", str]) -> bool:
        if isinstance(other, Site):
            return self.order.index(self.label) == self.order.index(other.label)
        else:
            return self.order.index(self.label) == self.order.index(other)

    def __lt__(self, other):
        return self.order.index(self.label) > self.order.index(other.label)

    def __hash__(self) -> int:
        return id(self.label)
