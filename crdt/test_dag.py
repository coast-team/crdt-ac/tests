import pytest
from typing import List, Tuple
from itertools import permutations

from .sites import Site
from .dag import DAG
from .op import Op
from .utils import correct_combination_op, DivergentBehaviour

ORDER = ["A", "B", "C"]

A = Site(label="A", order=ORDER)
B = Site(label="B", order=ORDER)
C = Site(label="C", order=ORDER)
D = Site(label="D", order=ORDER)

a1 = Op(site_from=A, site_to=D, num=1, right="write", sign="-", deps=["0"])
a2 = Op(site_from=A, site_to=D, num=2, right="write", sign="+", deps=[a1])
b1 = Op(site_from=B, site_to=D, num=1, right="write", sign="-", deps=["0"])
b2 = Op(site_from=B, site_to=D, num=2, right="write", sign="+", deps=[b1])
b3 = Op(site_from=B, site_to=D, num=3, right="write", sign="-", deps=[b2])
c1 = Op(site_from=C, site_to=D, num=1, right="write", sign="-", deps=["0"])
c2 = Op(site_from=C, site_to=D, num=2, right="write", sign="+", deps=[c1])


def test_dag__init():
    G = DAG(key="D")
    assert G.key == "D"
    assert G.ops == ["0"]
    assert G.pol == ["0"]
    assert G.doc == []


@pytest.fixture()
def G() -> DAG:
    return DAG(key="D")


def test_dag__effect(G: DAG):
    G.add_op("a1", ["0"])
    assert "a1" in G.ops
    assert G.estValide("a1")


def delivery_orders(operations: List[Op]) -> List[List[Op]]:
    return [list(t) for t in permutations(operations) if correct_combination_op(specs=list(t))]


def variants(delivery_orders: List[List[Op]], results: List[Op]):
    return [(k, [results]) for k in delivery_orders]


def ids(delivery_orders: List[List[Op]]) -> List[str]:
    return list(map(lambda x: ", ".join([str(y) for y in x]), delivery_orders))


def combinations(G: DAG, operations: List[Op], results: List[Op], order=ORDER):
    G.order = order

    # pour la combinaison courante, on itère pour simuler la réception dans l'ordre de la combinaison
    for i, op in enumerate(operations):
        G.add_op(str(op), list(map(lambda x: str(x), op.deps)))
        total = len(operations) - 1
        isFinal = i == total

        # pour l'itération i, on teste chacune des opérations "reçues"
        for o in operations[: i + 1]:
            result = G.estValide(str(o))
            if isFinal:
                assert result == (o in results) or DivergentBehaviour


ops = [a1, b1, b2]


@pytest.mark.parametrize("variant", variants(delivery_orders(ops), [a1, b1]), ids=ids(delivery_orders(ops)))
def test_dag__case_b(G: DAG, variant: Tuple[List[Op], List[Op]]):
    """On teste ici la résolution de conflits où une opération de niveau n prévaut sur une opération concurrente de niveau n + 1"""
    combinations(G, *variant)


ops = [a1, b1, b2]


@pytest.mark.parametrize("variant", variants(delivery_orders(ops), [b1, b2]), ids=ids(delivery_orders(ops)))
def test_dag__case_c__reverse_order(G: DAG, variant: Tuple[List[Op], List[Op]]):
    """On teste ici la résolution de conflits où une opération de niveau n + 1 prévaut sur une opération concurrente de niveau n. On le fait ici en changeant le critère discrétionnaire d'ordre de priorité des opérations"""
    combinations(G, *variant, order=ORDER[::-1])


ops = [a1, a2, b1, b2, c1, c2]


@pytest.mark.parametrize("variant", variants(delivery_orders(ops), [c1, c2]), ids=ids(delivery_orders(ops)))
def test_dag__case_d__reverse_order(G: DAG, variant: Tuple[List[Op], List[Op]]):
    """On teste ici une situation où l'opération priorisée est elle-même en conflit avec une troisième opération qui prend la priorité"""
    combinations(G, *variant, order=ORDER[::-1])


ops = [a1, b1, b2, b3]


@pytest.mark.parametrize("variant", variants(delivery_orders(ops), [a1, b1]), ids=ids(delivery_orders(ops)))
def test_dag__case_g(G: DAG, variant: Tuple[List[Op], List[Op]]):
    """Vérifie que l'algorithme fonctionne avec des niveaux plus importants"""
    combinations(G, *variant)


ops = [a1, a2, b1, b2, b3]


@pytest.mark.parametrize("variant", variants(delivery_orders(ops), [a1, a2]), ids=ids(delivery_orders(ops)))
def test_dag__case_h(G: DAG, variant: Tuple[List[Op], List[Op]]):
    """Vérifie que l'algorithme fonctionne avec des niveaux plus importants de chaque côté"""
    combinations(G, *variant)
