from typing import List, Tuple
from itertools import permutations

from .dag import DAG
from .crdt import Rights
from .utils import bool_vers_estValide, correct_combination


def batterie(specs: List[Tuple[str, DAG, bool]]):
    """Première itération d'une fonction testant une opération pour un résultat attendu"""
    for spec in specs:
        result = spec[1]._get_valide(spec[0])
        print("{:2}: {}{}".format(spec[0], bool_vers_estValide(result), " ❌" if result is not spec[2] else " ✅"))


from rich.console import Console
from rich.columns import Columns
from rich.panel import Panel
from rich.table import Table
from rich.text import Text
from rich.style import Style
from rich import box


def batterie_lisible(specs: List[Tuple[str, DAG, bool]]) -> Panel:
    """Seconde itération, cette fois avec un affichage en résumé et sous forme de Panel affichable en colonnes via Rich"""
    ops = "0," + ",".join([el[0] for el in specs])
    valid = True
    sequence = str("")

    for spec in specs:
        result = spec[1]._get_valide(spec[0])
        sequence += "\n{:2}: {}{}".format(spec[0], bool_vers_estValide(result), " ❌" if result is not spec[2] else " ✅")
        if result is not spec[2]:
            valid = False

    if valid:
        return Panel(f"[b][green]{ops}[/green][/b]{sequence}")
    else:
        return Panel(f"[b][red]{ops}[/b][/red]{sequence}")


def table(panels: List[Panel]):
    """Utilitaire d'affichage pour une liste de batterie_lisible"""
    console = Console()
    console.print(Columns(panels))


def test_combinations(specs: List[Tuple[str, Tuple, List[str], bool]], order=None, detail=False):
    """Troisième itération, génération toutes les suites possibles et évaluant leur convergence

    On spécifie `detail` si l'on souhaite lister les étapes intermédiaires d'évaluation à chaque opération reçue pour une combinaison (scenario)
    """
    console = Console()
    cells_batch = []

    # permutation pour chaque combinaison de séquence d'opération
    combinations = permutations(specs, len(specs))
    num_combinations = 0  # compteur du nombre total de combinaisons existantes
    num_correct_combinations = 0  # compteur du nombre total de combinaisons correctes, toutes les combinaisons ne pouvant pas respecter l'ordre de livraison
    num_valid_combinations = (
        0  # compteur du nombre total de combinaisons dont l'évaluation converge vers le résultat attendu
    )
    for j, comb in enumerate(combinations):
        num_combinations += 1
        if not correct_combination(list(comb)):
            continue
        else:
            num_correct_combinations += 1

        cells = []
        ops = str("")
        R = Rights()

        # pour la combinaison courante, on itère pour simuler la réception dans l'ordre de la combinaison
        for i, spec in enumerate(comb):
            # on ajoute à R registre des DAG de politique les DAG lorsqu'ils sont listés dans une spec pour la première fois
            if spec[1] not in R:
                R[spec[1]] = DAG()
                if order:
                    R[spec[1]].order = order

            # on ajoute l'opération à son DAG
            R[spec[1]].add_op(spec[0], spec[2])
            ops = "0," + ",".join([el[0] for el in comb[: i + 1]])
            isValid = True
            total = len(comb) - 1
            isFinal = i is total
            sequence = Table()
            sequence.box = box.SIMPLE
            sequence.show_header = False

            # pour l'itération i, on teste chacune des opérations "reçues"
            for s in comb[: i + 1]:
                dag = s[1]
                node = s[0]
                expected = s[3]
                result = R[dag].estValide(node)
                sequence.add_row(
                    node,
                    "{}{}".format(
                        bool_vers_estValide(result),
                        (" ❌" if result is not expected else " ✅") if isFinal else "",
                    ),
                )
                if result is not expected:
                    isValid = False

            validStyle = Style(color="green", bold=True)
            invalidStyle = Style(color="bright_red", bold=True)
            invalidTransientStyle = Style(color="dark_orange3", bold=True)

            if isValid:
                cells.append(
                    Panel(
                        sequence,
                        title=f"[yellow]#{j}[/] FINAL STATE" if isFinal else f"STEP {i+1}/{total+1}",
                        subtitle=Text(ops, style=validStyle),
                    )
                )
            else:
                cells.append(
                    Panel(
                        sequence,
                        title=f"[yellow]#{j}[/] FINAL STATE" if isFinal else f"STEP {i+1}/{total+1}",
                        subtitle=Text(ops, style=invalidStyle if isFinal else invalidTransientStyle),
                    )
                )
            if isFinal and isValid:
                num_valid_combinations += 1

        if detail:
            console.print(
                Columns(
                    cells,
                    padding=(1, 1),
                    title="Evaluation after each reception (scenario [yellow]#{}[/]: {})\n".format(j, ops),
                )
            )
        else:
            cells_batch.append(cells.pop())

    if not detail:
        console.print(
            Columns(
                cells_batch,
                padding=(1, 1),
                title="Final evaluation of each scenario ({} valides/{} correctes/{} permutations)\n".format(
                    num_valid_combinations, num_correct_combinations, num_combinations
                ),
            )
        )
