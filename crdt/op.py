from dataclasses import dataclass, field
from typing_extensions import List, TypeVar, Generic, Optional, Literal, Self, overload
from .sites import Site


Dot = TypeVar("Dot", "Op", str)


@dataclass(repr=False)
class Op(Generic[Dot]):
    site_from: Site
    num: int
    site_to: Site
    right: Literal["admin", "write", "read"]
    sign: Literal["+", "-"]
    deps: List[Dot] = field(default_factory=list)
    last: Optional[Dot] = None
    missing: Optional[List[Dot]] = field(default_factory=list)

    def __repr__(self) -> str:
        return f"{self.site_from.label.lower()}{self.num}"

    def __eq__(self, other: Self | str) -> bool:
        if isinstance(other, Self) or isinstance(other, str):
            return str(self) == str(other)
        return NotImplemented

    @property
    def tuple(self):
        return (self.site_to.label, self.right)
