from typing import List, Tuple, TypeVar, Any
from abc import ABC, abstractmethod

from .op import Op

T = TypeVar("T")


class DivergentBehaviour(Exception):
    """The algorithm irremediably diverges from the expected state"""


class OverfullWaitingOperations(DivergentBehaviour):
    """The waiting pool of unprocessed operations is not empty at the end of the integration cycle"""


class UnmanagedOperation(Exception):
    """The selected operation is not part of the current access tuple"""


class UnmanagedDocumentOperation(UnmanagedOperation):
    """The selected operation is not managed by the current access tuple, which can't verify its validity"""


def flatten(l):
    return [item for sublist in l for item in sublist]


def opnum(node: str) -> int:
    try:
        return int(node[1:])
    except ValueError:
        return 0


def bool_vers_estValide(b: bool) -> str:
    """Utilitaire d'affichage de texte"""
    return "{:>12}".format("authorized") if b else "{:>12}".format("unauthorized")


def correct_combination_op(specs: List[Op]) -> bool:
    print(specs)
    for i, spec in enumerate(specs):
        ops_present = ["0"] + specs[: i + 1]
        if not all(x in ops_present for x in spec.deps):
            return False
    return True


def correct_combination(specs: List[Tuple[T, Any, List[T], Any]]) -> bool:
    """Utilitaire vérifiant qu'une suite d'opération est possible selon le modèle de livraison : les dépendances doivent précéder les opérations qui en dépendent"""
    for i, spec in enumerate(specs):
        ops_present = ["0"]
        deps = []
        if isinstance(spec[0], Op):
            deps = list(map(lambda x: str(x), spec[2]))
            ops_present += list(map(lambda x: str(x[0]), specs[: i + 1]))
        else:
            deps = spec[2]
            ops_present += list(map(lambda x: x[0], specs[: i + 1]))
        # print(" {} in {}".format(deps, ops_present))
        if not all(x in ops_present for x in deps):
            return False
    return True


def intermediates(p1, p2, nb_points=8):
    """Return a list of nb_points equally spaced points between p1 and p2"""
    # If we have 8 intermediate points, we have 8+1=9 spaces
    # between p1 and p2
    x_spacing = (p2[0] - p1[0]) / (nb_points + 1)
    y_spacing = (p2[1] - p1[1]) / (nb_points + 1)

    return [(p1[0] + i * x_spacing, p1[1] + i * y_spacing) for i in range(1, nb_points + 1)]
