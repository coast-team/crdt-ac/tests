"""
On définit ici l'utilisation du module python logging, et non un log des opérations au sens où on l'entend dans un CRDT
"""
import logging
import io


def dag_logger(name):
    _logger = logging.getLogger(name)
    _logger.setLevel(logging.DEBUG)

    # paramétrage du handler avec un objet StringIO
    log_string = io.StringIO()
    ch = logging.StreamHandler(log_string)
    ch.setLevel(logging.DEBUG)

    # ajout d'un formatteur
    # formatter = logging.Formatter("%(message)s")
    # ch.setFormatter(formatter)

    # ajout du handler au logger
    _logger.addHandler(ch)
    return _logger, log_string
