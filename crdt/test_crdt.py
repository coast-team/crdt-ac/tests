import pytest
from typing import List, Tuple, Generator
from itertools import permutations
from easycheck import check_if, assert_length

from .op import Op
from .sites import Site
from .crdt import CRDTp
from .utils import DivergentBehaviour, OverfullWaitingOperations

ORDER = ["A", "B", "C"]


def test_crdt__prepare():
    order = ["A", "B"]
    A = Site(label="A", order=order)
    B = Site(label="B", order=order)
    crdt = CRDTp(site_from=A)
    a1 = crdt.prepare(site_to=B, right="write")
    assert a1.__repr__() == "a1"
    assert a1.sign == "-"
    assert a1.last == None
    assert a1.deps == ["0"]


@pytest.fixture
def crdt_a() -> CRDTp:
    A = Site(label="A", order=ORDER)
    return CRDTp(site_from=A)


def test_crdt__effect(crdt_a: CRDTp):
    B = Site(label="B", order=ORDER)
    a1 = crdt_a.prepare(site_to=B, right="write")
    assert a1.__repr__() == "a1"  # on vérifie que a1 est n'a pas été incrémenté depuis le précédent test
    assert crdt_a.ac_dags.__len__() == 4  # A {admin,write,read} + B {write}
    assert a1.tuple == ("B", "write")
    dag = crdt_a.ac_dags[a1.tuple]  # B {write}
    ret = crdt_a.effect(a1)
    assert ret == [a1]
    assert str(a1) in dag.pol
    assert dag.estValide("a1")


def delivery_orders(operations: List[Op]) -> List[List[Op]]:
    return [list(t) for t in permutations(operations)]


def variants(delivery_orders: List[List[Op]], results: List[Op]):
    return [(k, results) for k in delivery_orders]


def ids(delivery_orders: List[List[Op]]) -> List[str]:
    return list(map(lambda x: ", ".join([str(y) for y in x]), delivery_orders))


def evaluation(crdt_a: CRDTp, operations: List[Op]) -> Generator[Tuple[Op, bool], Op, List[Tuple[Op, bool]]]:
    evaluated = []

    for op in operations:
        crdt_a.effect(op)

    assert_length(crdt_a.waiting_pool, 0, OverfullWaitingOperations, f"The waiting pool of unprocessed operations is not empty at the end of the integration cycle : {crdt_a.waiting_pool}")  # type: ignore

    for op in operations:
        result = crdt_a.ac_dags[op.tuple].estValide(str(op))
        evaluated.append((op, result))
        yield op, result

    return evaluated


class TestCRDT:
    A = Site(label="A", order=ORDER)
    B = Site(label="B", order=ORDER)
    crdt_a = CRDTp(site_from=A)
    D = Site(label="D", order=ORDER)
    a1 = crdt_a.prepare(site_to=D, right="write")
    a2 = Op(site_from=A, site_to=D, num=2, right="write", sign="+", deps=[a1])
    b1 = Op(site_from=B, site_to=D, num=1, right="write", sign="-", deps=["0"])
    b2 = Op(site_from=B, site_to=D, num=2, right="write", sign="+", deps=[b1])
    X = Site(label="X", order=ORDER)
    x1 = Op(site_from=X, site_to=A, num=1, right="admin", sign="-", deps=["0"])
    ops = [a1, b1, b2]

    @pytest.mark.parametrize("variant", variants(delivery_orders(ops), [a1, b1]), ids=ids(delivery_orders(ops)))
    def test__case_b__simple(self, variant: Tuple[List[Op], List[Op]]):
        crdt = CRDTp(site_from=self.A)
        expected = variant[1]
        for i in evaluation(crdt, variant[0]):
            op = i[0]
            result = i[1]
            check_if(result == (op in expected), DivergentBehaviour, f"(expected {expected} to be valid, got {op} {'valid' if result else 'invalid'})")  # type: ignore
        check_if((length := len(nodes := crdt.ac_dags.nodes)) == 4, message=f"{nodes} of length {length} ≠ 4")

    ops = [a1, b1, b2, x1]

    @pytest.mark.parametrize("variant", variants(delivery_orders(ops), [b1, b2, x1]), ids=ids(delivery_orders(ops)))
    def test__case_e__with_admin(self, variant: Tuple[List[Op], List[Op]]):
        crdt = CRDTp(site_from=self.A)
        expected = variant[1]
        for i in evaluation(crdt, variant[0]):
            op = i[0]
            result = i[1]
            check_if(result == (op in expected), DivergentBehaviour, f"(expected {expected} to be valid, got {op} {'valid' if result else 'invalid'})")  # type: ignore
        check_if((length := len(nodes := crdt.ac_dags.nodes)) == 5, message=f"{nodes} of length {length} ≠ 5")
