from __future__ import annotations
import logging
import io
import math
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
from functools import wraps
from typing import List, Any, TypeVar, Literal
from itertools import groupby
from copy import deepcopy
import matplotlib.pyplot as plt
from matplotlib.axes._axes import _log as matplotlib_axes_logger  # type: ignore
import portion as P

matplotlib_axes_logger.setLevel("ERROR")
from .utils import intermediates, opnum, flatten, UnmanagedOperation, UnmanagedDocumentOperation
from .logging import dag_logger
from .sites import Site
from . import NODE_SIZE, DOCUMENT_NODE_SIZE

T = TypeVar("T")


class DAG(nx.DiGraph):
    """Classe responsable d'un tuple d'accès de CRDT de politique

    Attributes:
    -----------
        order (list[str]): l'ordre de résolution site à site dans ce DAG
        starting_sign (int): the Stock-Keeping Unit number for the product
        key: identifiant du tuple d'accès, e.g. ('A', 'write')
        rights: référence vers la liste des dag (None si le DAG est utilisé hors d'une classe Rights)

    Méthodes (initialisation):
    --------------------------
        add_op: ajout d'une opération au DAG

    Méthodes (évaluation):
    ----------------------
        concurrents: liste les nœuds en concurrence avec un nœud donné
        conflits: list les nœuds en conflit avec un nœud donné
        résolution: indique si un nœud donné gagne un conflit avec un second nœud
        estValide: évalue si le nœud est valide
        getValide: donne la valeur sauvegardée d'une exécution précédente de estValide pour un nœud donné
        getSign: donne le signe d'une opération
        getLast: donne le numéro de dernière opération vue par un nœud
        aLeDroit: évalue si une opération gérée par le tuple d'accès est autorisée

    Méthodes (représentation):
    --------------------------
        draw: dessine le tuple selon une représentation sous forme d'arbre
    """

    order: list[str] = ["A", "B", "C"]
    starting_sign = "+"
    key = None
    compteur_reception = 0
    rights: Any = None
    logger: logging.Logger
    logger_string: io.StringIO
    logger_indent = 0

    def __repr__(self):
        return f"{self.__class__.__name__}([{self.starting_sign}{self.key}]-[{','.join(list(self.ops))}])"

    def __init__(self, incoming_graph_data=None, name=None, **attr):
        super().__init__(incoming_graph_data, **attr)
        self.logger, self.logger_string = dag_logger(name if name else repr(self))
        self.add_node("0")
        self.key = attr.get("key")

    # === Decorators ===

    @staticmethod
    def logged(*args, **kwargs):  # ligne correspondant à celle dans la spécification
        """Décorateur d'aide au log d'opérations pour faire correspondre la sortie à la spécification"""
        name = kwargs.get("name")
        line = kwargs.get("line")

        def decorator(func, *args, **kwargs):
            @wraps(func)
            def wrapper(self, *args, **kwargs):
                self.log(f"{name or func.__name__}({', '.join([str(a) for a in args])})", ligne=line)
                self.logger_indent += 1
                result = func(self, *args, **kwargs)
                self.logger_indent = max(0, self.logger_indent - 1)
                return result

            return wrapper

        return decorator

    # === Getters ===

    @property
    def ops(self):
        """Opérations"""
        return self._ordre_reception(
            [n for n in self.nodes if n[0] != "_"]
        )  # on possède aussi des nœuds qui ne sont pas des opérations : ce sont des nœuds utiles pour `draw()`

    @property
    def pol(self) -> List[str]:
        """Opérations de politique"""
        return [n for n in self.ops if n[0].capitalize() != self.key]

    @property
    @logged(name="pol_invalides")
    def pol_invalides(self):
        """Opérations de politique invalides"""
        _pol_invalides = [node for node in self.pol if not self.estValide(node)]
        self.log(f"ret {_pol_invalides}")
        return _pol_invalides

    @property
    def doc(self):
        """Opérations de document"""
        if self.rights and self.key and self.key[1] == "admin":
            return [
                n
                for n in flatten([dag.ops for dag in self.rights.values() if dag.key != self.key])
                if n.startswith(self.key[0].lower())
            ]
        return [n for n in self.ops if n[0].capitalize() == self.key]

    @property
    @logged(name="doc_invalides")
    def doc_invalides(self):
        """Opérations de document invalides"""
        _doc_invalides = [node for node in self.doc if not self.aLeDroit(node)]
        self.log(f"ret {_doc_invalides}")
        return _doc_invalides

    # === Méthodes ===

    @logged(name="effect", line="1.28")
    def add_op(self, name, deps, last=None, missing=[]):
        """Fonction d'intégration d'une opération, et de gestion de son effet sur le DAG

        Parameters
        ----------
        name : str
            Dot de l'opération (lettre minuscule du site, numéro de l'opération du site)
        deps : (List[str])
            liste des opérations valides précèdant l'opération lors de sa création
        last : (str, optional)
            dernière opération gérée par le tuple d'accès de l'opération avant le changement de l'opération. Defaults to None.
        missing : (List[str], optional)
            exception à l'intervalle précédent. Defaults to [].
        """
        # log
        for dep in deps:
            self.log("G.addEdge({})".format(f"({dep}) \u2192 ({name})"), ligne="1.29")
            self.add_edge(dep, name)
        self.log("D[{}] = <{} (LastDotSeen), {} (MissingDots)>".format(name, last, missing), ligne="1.30")

        # core
        site_letter = name[0].capitalize()
        nx.set_node_attributes(self, {name: Site(label=site_letter, order=self.order)}, "site")
        nx.set_node_attributes(self, {name: last}, "last")
        nx.set_node_attributes(self, {name: missing}, "missing")
        nx.set_node_attributes(self, {name: self.compteur_reception}, "ordre_reception")

        # invalidation du cache
        for node in self.nodes:
            self.nodes[node].pop("valide", None)

        # réception
        self.compteur_reception += 1

    def concurrents(self, node_a: T) -> List[T]:
        """Liste les nœuds en concurrence avec un nœud donné"""
        H = [n for n in self.nodes if n not in self._ancestors(node_a)]
        H.remove(node_a)  # list.remove
        return [node_b for node_b in H if node_a not in self._ancestors(node_b)]

    def conflits(self, node_a: T) -> List[T]:
        """Liste les nœuds en conflit avec un nœud donné"""
        return [
            node_b
            for node_b in self.concurrents(node_a)
            if node_a in self.pol
            if self._parity(node_a) is not self._parity(node_b)
        ]

    @logged(ligne="1.36")
    def estValide(self, node: str) -> bool:
        """Vérifie si une opération de politique du tuple d'accès est valide"""
        if node not in self.pol:
            raise UnmanagedOperation

        # core
        _concurrents = [c for c in self.concurrents(node) if c in self.ops]
        _conflits = self.conflits(node)
        gagne_conflits = (
            # on doit vérifier validité de o' quand o' > o, o' in conflits
            not any([self._get_valide(_conflit) for _conflit in _conflits if self._resolution(_conflit, node)])
            if len(_conflits) > 0
            else True
        )

        # log
        if node in self.ops:
            self.log(f"concurrents de {node} : {_concurrents or '∅'}", ligne="1.37")
            self.log(f"conflits de {node} : {_conflits or '∅'}", ligne="1.38")
            if len(_conflits) > 0:
                self.log(
                    f"conflits perdus : {[_conflit for _conflit in _conflits if self._resolution(_conflit, node) and self._get_valide(_conflit)] or '∅'}",
                    ligne="1.39",
                )

        predecessors = list(self.predecessors(node))

        # log
        if node in self.ops:
            self.log(f"vérification de validité des parents ({predecessors}) de {node}", ligne="1.39")

        parents_valides = all([self._get_valide(pred) for pred in predecessors]) if len(predecessors) > 0 else True

        # évaluation de la validité de l'opération dans son graphe d'admin via self.rights
        admin_valide_pour_node = True
        if getattr(self, "rights") is not None:
            try:
                label = self.nodes[node]["site"].label
                graphe_admin_de_node: DAG = self.rights[(label, "admin")]
                if graphe_admin_de_node and node in graphe_admin_de_node.doc:
                    admin_valide_pour_node = graphe_admin_de_node.aLeDroit(node)
            except (
                KeyError
            ):  # pour facliter la mise en place des tests, on part du principe que l'état initial autorise le droit admin d'un site si ce droit n'est pas précisé
                pass

        valide = gagne_conflits and parents_valides and admin_valide_pour_node
        nx.set_node_attributes(self, {node: valide}, "valide")

        # log
        if node in self.ops:
            raison = f" \u2192 (\u2227 conflits gagnés {':green_circle:' if gagne_conflits else ':red_circle:'}) \u2227 (\u2227 prédecesseurs directs valides {':green_circle:' if parents_valides else ':red_circle:'})"
            self.log(f"ret {'[green]valide[/]' if valide else f'[red bold]invalide[/]{raison}'}", ligne="1.39")

        return valide

    @logged(name="estValide", line="2.17")
    def aLeDroit(self, node: str) -> bool:
        """Vérifie si une opération gérée par le tuple d'accès est valide"""
        if self.key and not node.startswith(self.key[0].lower()):
            raise UnmanagedDocumentOperation

        num = opnum(node)
        lasts: dict[str, str] = nx.get_node_attributes(self, "last")  # pour chaque node, son dernier node vu
        missings: dict[str, List[str]] = nx.get_node_attributes(
            self, "missing"
        )  # pour chaque node, sa liste de node exclus
        lasts_filtered = {
            k: v for k, v in lasts.items() if (k in self.pol and k not in self.pol_invalides)
        }  # lasts réduit aux opérations de politique valides seulement
        missings_filtered = {
            k: v for k, v in missings.items() if (k in self.pol and k not in self.pol_invalides)
        }  # idem pour missings
        nodes_excluded = [n for n in missings_filtered.values() if n]

        if node in self.nodes:  # l'opération 'node' fait partie du tuple d'accès courant
            """La différence majeure avec le cas où le 'node' est externe se situe dans le fait que l'on peut ici calculer le niveau
            de l'opération, là où un 'node' externe sera jugé sur la seule base de son numéro d'opération par rapport à l'intervalle
            de validité.

            Plutôt que de calculer tous les intervalles de validité du tuple d'accès, on peut se permettre ici de ne calculer que
            celui relatif au niveau de l'opération.
            """

            # setup
            default = "0"  # FIXME: devrait prendre la valeur de last du parent ?
            level = self._level(node) - 1
            suivants = [
                x for x in self.pol if self._level(x) == level + 1
            ]  # seulement les opérations de politique suivantes
            lasts_filtered = {
                k: v for k, v in lasts_filtered.items() if (k in suivants)
            }  # lasts_filtered réduit à aux suivants seulement
            missings_filtered = {k: v for k, v in missings_filtered.items() if (k in suivants)}  # idem pour missings
            nodes_excluded = flatten(missings_filtered.values())
            borne_haute = (
                min(lasts_filtered.values(), key=lambda x: opnum(x) if x else math.inf) if lasts_filtered else None
            )

            # log
            _suivants_valides = [
                x for x in suivants if x not in self.pol_invalides
            ]  # seulement les opérations de politique suivantes valides
            self.log(f"niveau : {level}", ligne="2.12")
            self.log(f"  listerNiveau({level + 1})", ligne="2.14")
            self.log(f"suivants : {suivants}", ligne="2.18")
            self.log(f"suivants_valides : {_suivants_valides}", ligne="2.19")
            self.log(f"borne_haute : min({lasts_filtered}) = {borne_haute}", ligne="2.20")
            self.log(f"exclusions : {missings_filtered} = {nodes_excluded}", ligne="2.21")

            # core
            if node not in nodes_excluded:
                for k, v in lasts_filtered.items():
                    v = int(opnum(v) if v else default)
                    if self._get_sign(k) == "+" and v < num:  # après ajout de droit
                        if node in self.ops:
                            self.log(f"ret {'[green]valide[/]'}")
                        return True
                    if self._get_sign(k) == "-" and v >= num:  # avant retrait de droit
                        if node in self.ops:
                            self.log(f"ret {'[green]valide[/]'}")
                        return True

            parents_valides = True

            # log
            exclusion_raison = (
                "\u2209 exclusions :green_circle:" if node not in nodes_excluded else "\u2208 exclusions :red_circle:"
            )
            raison = f" \u2192 ({node} {exclusion_raison}) \u2227 (ID_Source ({node}) < borne_haute {borne_haute} {':green_circle:' if opnum(node) < (opnum(borne_haute) if borne_haute else math.inf) else ':red_circle:'}) \u2227 (\u2227 prédecesseurs directs valides {':green_circle:' if parents_valides else ':red_circle:'})"
            self.log(f"ret {'[red]invalide[/]'}{raison}", ligne="2.22")
        elif (
            node not in nodes_excluded
        ):  # l'opération 'node' fait partie d'un autre tuple d'accès, mais est gérée par le tuple d'accès courant pour le droit 'admin'
            # setup
            intervals: List[P.Interval] = []
            length = self._longest_valid_path_length()

            # core
            if length == 0 and self.starting_sign == "+":
                return True

            for i in range(1, length + 1):
                if (i % 2 and self.starting_sign == "-") or (
                    not i % 2 and self.starting_sign == "+"
                ):  # i est un niveau d'autorisation
                    get = lambda λ: [n for n in self._level_nodes(λ) if n in self.pol and n not in self.pol_invalides]
                    previous_level_nodes = get(i - 1)
                    level_nodes = get(i)
                    # récupération de la borne parmi celles déclarées sur un niveau
                    bound = lambda λ: max(map(lambda x: lasts_filtered[x], λ), default=0) or 0
                    intervals.append(P.openclosed(lower=bound(previous_level_nodes), upper=bound(level_nodes)))

            if any([num in validity_interval for validity_interval in intervals]):
                return True

        return False

    def eval(self) -> str:
        return self._longest_valid_path()[-1]

    # === Méthodes protégées ===

    def _get_valide(self, node: Any) -> bool:
        """Utilisation du cache plutôt que de recalculer tout. Le cache est invalidé lors d'un ajout via `add_node`."""
        cache = nx.get_node_attributes(self, "valide")
        hit = cache.get(node)
        if hit is None:
            return self.estValide(node)
        else:
            self.log(
                f"estValide({node}) \u2192 {'[green]valide[/]' if hit else f'[red bold]invalide[/]'}", ligne="cache"
            )
            return hit

    def _get_sign(self, node: Any) -> Literal["+", "-"]:
        """Retourne le signe (+ est un ajout de droit, - un retrait) d'une opération"""
        if self._parity(node) and self.starting_sign == "+":
            return "-"
        if self._parity(node) and self.starting_sign == "-":
            return "+"
        if not self._parity(node) and self.starting_sign == "+":
            return "+"
        else:  # if not self.parité(node) and self.starting_sign is '-'
            return "-"

    def _get_last(self, node: T) -> T:
        """Retourne le Dot de la dernière opération gérée par le tuple vue par l'opération de politique donnée"""
        lasts = nx.get_node_attributes(self, "last")
        return lasts[node]

    def _resolution(self, node_a: T, node_b: T) -> bool:
        """Retourne le test : est-ce que 'node_a' l'emporte sur 'node_b' ?"""
        site = nx.get_node_attributes(self, "site")
        # on peut imaginer d'autres stratégies…
        return site[node_a] > site[node_b]

    @logged()
    def _longest_valid_path(self) -> List[str]:
        _self = deepcopy(self)
        _self.remove_nodes_from(_self.doc)
        last_path = nx.dag_longest_path(_self)
        last = last_path[-1]
        while (last not in self.ops) or (not self.estValide(last)):
            _self.remove_node(last)
            last_path = nx.dag_longest_path(_self)
            last = last_path[-1]
            self.log(f"calcul pour {last}")  # log
        self.log(f"ret {last_path}")
        return last_path

    def _longest_valid_path_length(self) -> int:
        path = self._longest_valid_path()
        return len(list(zip(path, path[1:])))

    def _ancestors(self, node: T) -> List[T]:
        """DiGraph.predecessors ne retourne que les ancètres de niveau 1 (immédiatement avant)"""
        predecessors = [pred for pred in self.predecessors(node)]
        return predecessors + flatten([self._ancestors(pred) for pred in predecessors])

    def _level(self, node: Any) -> int:
        path = nx.shortest_path(self.to_directed(), source="0", target=node)
        return len(list(zip(path, path[1:])))

    def _level_nodes(self, niveau: int) -> List[str]:
        return [_node for _node in self.ops if self._level(_node) == niveau]

    def _parity(self, node: Any) -> int:
        return self._level(node) % 2

    # === Méthode de réception ===

    def _ordre_reception(self, nodes: List[T]) -> List[T]:
        ordre = nx.get_node_attributes(self, "ordre_reception")
        return sorted(nodes, key=lambda n: ordre.get(n, -1))

    def _ordre_dot(self, nodes: List[str]) -> List[str]:
        return sorted(nodes)

    # === Méthodes de log ===

    def log(self, text, ligne=None):
        self.logger.info("{:<5}{}{}".format(ligne if ligne else "", " " * self.logger_indent * 2, text))

    def get_log(self) -> str:
        return self.logger_string.getvalue()

    def pop_log(self) -> str:
        _log = self.get_log()
        self.logger_string.truncate(0)
        return _log

    # === Méthodes de dessin ===

    def draw(self, avec_doc=True):
        # on ajoute les nodes "underscore" qui viennent donner un edge sur lequel ajouter les opérations de document en boût de chemin
        for _, pol in enumerate(self.__last_pol_with_doc_after()):
            if f"_{pol}" in self.nodes:
                continue
            self.add_node(f"_{pol}")
            self.add_edge(pol, f"_{pol}")

        # calcul des positions de tous les nœuds (sauf doc) et dessin
        nodes = (
            [x for x in self.nodes if x not in self.doc] if avec_doc else self.pol
        )  # on doit inclure les nœuds "underscore" qui ne sont pas des opérations
        P = self.subgraph(nodes)
        pos = graphviz_layout(P, prog="dot")
        D, posDocuments = self.__get_documents_to_draw(pos) if avec_doc else (None, None)
        nx.draw(P, pos, node_color="gray", font_color="white", node_size=NODE_SIZE, with_labels=True)

        # labels avec les signes '+' et '-'
        labels, posLabels = self.__get_labels_to_draw(pos)
        nx.draw_networkx_labels(P, posLabels, labels=labels, font_size=14, font_weight="10")

        # opérations pol invalides en rouge
        nx.draw_networkx_nodes(
            P, pos, nodelist=self.pol_invalides, node_size=NODE_SIZE, node_color="r", #label="unauthorized operations"
        )

        # chemin le plus long en vert
        path = self._longest_valid_path()
        nx.draw_networkx_nodes(
            P, pos, nodelist=path, node_size=NODE_SIZE, node_color="g", #label="operations of the longuest authorized arc"
        )
        path_edges = list(zip(path, path[1:]))
        nx.draw_networkx_edges(P, pos, edgelist=path_edges, edge_color="#4ACC43", width=5)

        # on dessine les opérations de document
        if avec_doc:
            doc_colors = [self.__get_node_color(x) for x in self.doc]
            nx.draw(
                D,
                posDocuments,
                nodelist=self.doc,
                node_color=doc_colors,
                node_shape="d",
                font_color="white",
                label="document operations",
                node_size=DOCUMENT_NODE_SIZE,
                with_labels=True,
                font_size=10,
            )
            underscores = [x for x in self.nodes if x not in self.ops]
            nx.draw_networkx_nodes(
                P, pos, nodelist=underscores, node_size=NODE_SIZE + 50, node_color="white"
            )  # on peint en blanc les nœuds "underscore"

        plt.title(
            label=",".join(self.ops), fontweight=10, loc="left"
        )  # ordre des opérations reçues affiché en haut à gauche
        plt.legend()
        plt.show()

    @logged()
    def __get_node_color(self, node: str) -> str:
        self.log(f"__get_node_color({node})")
        self.logger_indent += 1
        if node in self.pol:
            if not self.estValide(node):
                return "red"
            if node in self._longest_valid_path():
                return "green"
        if node in self.doc:
            if self.aLeDroit(node):
                return "#348F2F"
            return "#E6512C"
        return "gray"

    def __firstPredecessor(self, node: str) -> str:
        return list(self.predecessors(node))[0]

    def __firstSuccessor(self, node: str) -> str:
        P = self.subgraph(self.pol)
        niveau = self._level(node)
        predecessor = self.__firstPredecessor(node)
        try:
            if niveau > self._longest_valid_path_length():
                successors = dict(nx.bfs_successors(P, source=predecessor))
                return str(successors[predecessor][0])
            else:
                return {self._level(op): op for op in self._longest_valid_path()}[niveau]
        except (KeyError, IndexError):
            return f"_{predecessor}"  # le node doit être ajouté dynamiquement dans draw

    def __last_pol_with_doc_after(self) -> List[str]:
        return [
            x
            for x in [  # opérations de politique avec un degré sortant non nul (ont des ops dépendant d'elles)
                z for z in self.pol if self.out_degree(z) != 0
            ]
            if all(  # parmi ces opérations, lesquelles n'ont QUE des doc dépendant d'elles
                y in self.doc for y in self.successors(x)
            )
        ]

    def __get_documents_to_draw(self, pos):
        D = self.subgraph(self.doc)
        doc_par_parent = [
            list(g) for _, g in groupby(self._ordre_dot(D.nodes), lambda x: list(self.predecessors(x))[0])
        ]
        posDocuments = {}
        for d_list in doc_par_parent:
            node = d_list[0]
            prev_pol = self.__firstPredecessor(node)
            next_pol = self.__firstSuccessor(node)
            inter = intermediates(pos[prev_pol], pos[next_pol], nb_points=len(d_list))
            for i, d_el in enumerate(d_list):
                posDocuments[d_el] = inter[i]

        return D, posDocuments

    def __get_labels_to_draw(self, pos):
        labels = {k: v for (k, v) in map(lambda x: (x, self._get_sign(x)), self.pol)}
        posLabels = deepcopy(pos)
        for p in posLabels:
            yOffSet = 0
            xOffSet = 5
            posLabels[p] = (posLabels[p][0] + xOffSet, posLabels[p][1] + yOffSet)
        return labels, posLabels
