from .utils import opnum, correct_combination


def test_opnum():
    assert opnum("a1") == 1
    assert opnum("0") == 0


def test_correct_combination():
    assert correct_combination(specs=[("a2", None, ["a1"], None), ("a1", None, ["0"], None)]) == False
    assert correct_combination(specs=[("a1", None, ["0"], None), ("a2", None, ["a1"], None)]) == True
